This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Social Data Indexer Smart Executor Plugin

## [v4.0.0]

- Communicate with persistence through social networking service

## [v3.1.0-SNAPSHOT]

- Upgraded gcube-bom version
- Upgraded guava-version


## [v3.0.0]

- Ported plugin to smart-executor APIs 3.0.0 [#21570]


## [v2.0.0]

- 


## [v1.2.1] [2017-05-01]

- Minor fixes</Change>


## [v1.2.0] [2017-02-01]

- Added notification mechanism in case of failures


## [v1.1.1] [2016-10-20]

- Astyanax dependency version updated: moved to 2.0.2 (as declared by Portal-BOM)


## [v1.1.0] [2016-07-01]

- Minor fix


## [v1.0.0] [2016-02-29]

- First Release
